open Cmdliner
open Tezos_data_encoding
open Tezos_crypto

let vendor_id = 0x2c97
let product_id = 0x0001

module Human_readable = struct
  let hardened i = Int32.logand i 0x8000_0000l <> 0l
  let of_hardened = Int32.logand 0x7fff_ffffl
  let to_hardened = Int32.logor 0x8000_0000l

  let tezos_root = [to_hardened 44l ; to_hardened 1729l]

  let derivation_of_string d =
    match String.(get d (length d - 1)) with
    | '\'' ->
      let v = String.(sub d 0 (length d - 1)) |> Int32.of_string in
      to_hardened v
    | _ ->
      Int32.of_string d

  let string_of_derivation = function
    | i when hardened i -> Int32.to_string (of_hardened i) ^ "'"
    | i -> Int32.to_string i

  type t = Int32.t list

  let of_string_exn s =
    match String.split_on_char '/' s with
    | [""] -> []
    | derivations -> List.map derivation_of_string derivations
    | exception _ ->
        invalid_arg (Printf.sprintf "Human_readable.of_string_exn: got %S" s)

  let of_string s =
    try Some (of_string_exn s) with _ -> None

  let to_string t =
    List.map string_of_derivation t |>
    String.concat "/"

  let pp ppf t =
    Format.pp_print_string ppf (to_string t)

  let conv =
    Arg.conv
      ((fun s ->
          try Ok (Int32.of_string s)
          with _ ->
            let lens = String.length s in
            if String.get s (lens - 1) <> 'h' then
              Error (`Msg "Invalid BIP44 path")
            else
              try Ok (to_hardened (Int32.of_string (String.sub s 0 (pred lens))))
              with _ -> Error (`Msg "Invalid BIP44 path")),
       (Fmt.of_to_string string_of_derivation))
end

let get_wallet_pubkey path =
  let path = Human_readable.tezos_root @
             List.map Human_readable.to_hardened path in
  let h = Hidapi.open_id_exn ~vendor_id ~product_id in
  let pk_raw = Ledgerwallet_tezos.get_public_key h Ed25519 path in
  Cstruct.set_uint8 pk_raw 0 0 ; (* hackish, but works. *)
  let pk_raw_ba = Cstruct.to_bigarray pk_raw in
  let pk =
    Data_encoding.Binary.of_bytes_exn Signature.Public_key.encoding pk_raw_ba in
  let pk_signature =
    Cstruct.to_bigarray @@ Ledgerwallet_tezos.sign h Ed25519 path pk_raw in
  match Signature.(check pk (of_ed25519 (Ed25519.of_bytes_exn pk_signature)) pk_raw_ba) with
  | false ->
    Printf.eprintf "Invalid signature. Something went wrong." ;
    exit 1
  | true ->
    let pkh = Signature.Public_key.hash pk in
    Format.printf "%a@.%a@.%a@."
      Cstruct.hexdump_pp (Cstruct.shift pk_raw 1)
      Signature.Public_key_hash.pp pkh
      Signature.Public_key.pp pk

let cmd =
  let doc = "Get Ledger pubkey from a BIP44 path." in
  let path =
    Arg.(value & pos_right (-1) int32 [] & info [] ~docv:"BIP44-Path") in
  Term.(const get_wallet_pubkey $ path),
  Term.info ~doc "tezosledger"

let () = match Term.eval cmd with
  | `Error _ -> exit 1
  | #Term.result -> exit 0
